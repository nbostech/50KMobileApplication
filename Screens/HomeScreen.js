import React, { Component } from 'react';
import {
	Dimensions,
	StatusBar,
	StyleSheet,
	View,
} from 'react-native';
import { Container, Content, Header, Title, Footer, FooterTab, Button, Icon, Text, Badge, Left, Body, Right, StyleProvider, Tab, Tabs} from 'native-base';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';


import DealScreen from './DealScreen';
import EventScreen from './EventScreen';
import NewsScreen from './NewsScreen';
import ProfileScreen from './ProfileScreen';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class HomeScreen extends Component {
	constructor(props){
		super(props);

		this.state={
			page: 'Deals',
			showCarousel: true,
			isLoggedIn: false
		};
	
		
	}
	renderPage = (page)=>{
		if(this.state.page == "Deals"){
			
			return (<DealScreen theme={this.props.theme} navigator={this.props.navigator} info={this.props.info}/>);
		}
		if(this.state.page == "Events"){
			return (<EventScreen theme={this.props.theme} navigator={this.props.navigator} info={this.props.info}/>);
		}
		if(this.state.page == "News"){
			return (<NewsScreen theme={this.props.theme} navigator={this.props.navigator} info={this.props.info}/>);
		}
		if(this.state.page == "Profile"){
			return (<ProfileScreen theme={this.props.theme} navigator={this.props.navigator} info={this.props.info}/>);
		}
	}
	/*
	handleRequest(){
		//get the email from local storage and  set to null
		this.props.info.store('email', 'null') ;
			//navigate to landing screen
		this.props.info.reset('LandScreen', this.props.navigator)
		}
		*/
		
		componentWillMount(){
			/*this.props.info.retrieve('userLogginedForFirsttime').then((value)=>{
				console.log(value);
				this.setState({userLogginedForFirsttime: value});
			}); */
		}
		
			
  render() {

		const theme = this.props.theme;
		return (
			<StyleProvider style={getTheme(platform)}>
			<Container>
				<Header androidStatusBarColor={theme.themeColor} iosBarStyle="light-content"  zIndex={0}>
					<Left/>
					<Body>
						<Title style={{color: 'white'}}>{this.state.page}</Title>
					</Body>
					<Right>
						<Button transparent>
							{
							//if profile comes, show log-out the icon
							this.state.page == 'Profile'?
								<Icon name='log-out' ios="ios-log-out" android="md-log-out" style={{color: 'white'}}
								onPress={()=>{
									this.props.info.store('userId', 'null') ;
									this.props.info.reset('LoginScreenVersion', this.props.navigator)
										}
								}
								/>
								:
								<Icon name='search' ios="ios-search" android="md-search" style={{color: 'white'}}/>
							
							}
						</Button>
					</Right>
				</Header>
				<Content style={{backgroundColor: theme.backgroundColor}}  zIndex={0}>
					{
						this.renderPage()
					}
				</Content>
				
				
				<Footer  zIndex={0}>
					<FooterTab>
					<Button active={this.state.page == "Deals"?true:false} vertical onPress={()=>{this.setState({page: 'Deals'}); this.renderPage()}} >
						{/*Badge for Deals*/}
							{/*
							<Badge style={{backgroundColor: theme.themeColor}}>
								<Text>2</Text>
							</Badge>
						*/}
						<Icon android='logo-buffer' ios='logo-buffer' />
						<Text>Deals</Text>
					</Button>
					<Button active={this.state.page == "Events"?true:false} vertical onPress={()=>{this.setState({page: 'Events'}); this.renderPage()}}>
						{/*Badge for Events*/}
							{/*
							<Badge style={{backgroundColor: theme.themeColor}}>
								<Text>2</Text>
							</Badge>
							*/}
						<Icon android='md-calendar' ios='ios-calendar' />
						<Text>Events</Text>
					</Button>
					<Button active={this.state.page == "News"?true:false} vertical onPress={()=>{this.setState({page: 'News'}); this.renderPage()}}>
						{/*Badge for News*/}
							{/*
							<Badge style={{backgroundColor: theme.themeColor}}>
									<Text>2</Text>
							</Badge>
							*/}
						<Icon android='md-paper' ios='ios-paper' />
						<Text>News</Text>
					</Button>
					<Button active={this.state.page == "Profile"?true:false} vertical onPress={()=>{this.setState({page: 'Profile'}); this.renderPage()}}>
						<Icon android='md-settings' ios='ios-settings' />
						<Text>Profile</Text>
					</Button>
					</FooterTab>
				</Footer>
					{/*layer of violet*/}
					{
					this.state.showCarousel && 
					<View style={{flex: 1, position: 'absolute', width: deviceWidth, height: deviceHeight, top: 0, left: 0, bottom: 0, right: 0, padding: 20, backgroundColor: 'gray', opacity: 0.5}} zIndex={1}></View>
					}
					{/* A pop up for disclaimer*/}
				
					{
						this.state.showCarousel &&
						<View style={{flex: 1, position: 'absolute', width: deviceWidth, height: deviceHeight, top: 0, left: 0, bottom: 0, right: 0,padding: 10, flexDirection: 'column', justifyContent:'center'}} zIndex={2}>
						
							<View style={{backgroundColor: 'white', padding: 20, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', borderRadius: 10, opacity: 1.0, height: 3.5*deviceHeight/5}}>
								<View style={{margin: 10}}>
									<Text style={{color: '#9b9b9b', fontSize: 30, fontFamily: theme.fontFamily, textAlign: 'center'}}>Disclaimer!</Text>
								</View>
								<View style={{margin: 10}}>
									<Text style={{color: '#9b9b9b', fontSize: 18, fontFamily: theme.fontFamily, textAlign: 'center'}}>50K Ventures is not a Stock Exchange nor does it intend to get recognized as a stock exchange under Securities Contracts Regulation Act, 1956. 50K does not facilitate any online or offline buying, selling or dealing in securities, clearing or settlement of trades of securities and is not a network or a platform which facilitates secondary market trading.</Text>
								</View>
							
								<View style={{margin: 10}}>
									<Button style={{backgroundColor: '#e5e5e5' }} rounded onPress={()=> {
										console.log("OK clicked!");
										this.setState({showCarousel: false})
										}}>
										<Text style={{color:'black', fontFamily: theme.fontFamily}}>OK</Text>
									</Button>
								</View>
							</View>
						</View>
					}
			
			</Container>
			
		
			</StyleProvider>
		);
  }
}
