//https://stackoverflow.com/questions/39682445/prevent-webview-from-loading-url-in-android-react-native
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Animated,
    Dimensions,
    TextInput,
    TouchableOpacity,
    Platform,
    ActivityIndicator,
    Alert

} from 'react-native';
import { Button, Item, Input, Icon } from 'native-base';

//import config
import {config} from '../config.js';

//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const emailUrl = config.loginUrl;

class FadeInView extends React.Component {
    state = {
        fadeAnim: new Animated.Value(0),

        // Initial value for opacity: 0
    }

    componentDidMount() {
        Animated.timing(                  // Animate over time
            this.state.fadeAnim,            // The animated value to drive
            {
                toValue: 1,                   // Animate to opacity: 1 (opaque)
                duration: 2000,              // Make it take a while
            }
        ).start();                        // Starts the animation
    }

    render() {
        let { fadeAnim } = this.state;

        return (
            <Animated.View                 // Special animatable View
                style={{
                    ...this.props.style,
                    opacity: fadeAnim,         // Bind opacity to animated value
                }}
            >
                {this.props.children}
            </Animated.View>
        );
    }
}
export default class LoginScreenForFirstVersion extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading: false,
            emailUrl: '',
            mobile:'',
            email:'',
        }

        this.verifyEmailAndMobileForSignIn = this.verifyEmailAndMobileForSignIn.bind(this);
    }
    componentWillMount() {

    }

        verifyEmailAndMobileForSignIn(){
        var url = emailUrl + this.state.email +"/"+ this.state.mobile;
        console.log(url);
        fetch(url)
            .then((response) =>{
                console.log(response);
                if(response.status == 200){
                    this.setState({isLoading: true});
                    return response.json()
                }
                else if(response.status == 404){
                     Alert.alert(
                        '',
                        'Your email and mobile is not registered with us. Please contact info@50kventures.com.',
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                    return null;
                }
                else if(response.status == 500) {
                    Alert.alert(
                        '',
                        'Internal Server Error.',
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                    return null;
                }
            })
            .then((responseJson) => {
                if(responseJson != null){
                    console.log(responseJson);
                    var userId = responseJson.id;

                    var userId = userId.toString();
                    console.log(userId);
                     this.props.info.store('userId',userId );
                    console.log(userId);
                    console.log("navigate to Home screen")
                    //response sent successfully, send the user to home screen
                    this.props.info.reset('Home', this.props.navigator);
                }


            })
            .catch((error) => {
                console.error(error);
            });
    }

    render()
    {
       // console.log(this.props);
        const theme = this.props.theme;
        const {goBack} = this.props.navigator;


        return (

            <View style={{flex: 1, backgroundColor:'white', flexDirection:'column', justifyContent: 'space-around', alignItems: 'center'}}>

                {/*Header*/}
                {/*<View style={{flex: 0, alignSelf: 'flex-start', marginLeft: 20, marginTop: (Platform.OS === 'ios'? 25: 15)}}>
                    <TouchableOpacity onPress={()=> goBack()}>
                        <Icon ios='ios-arrow-back' android="md-arrow-back" style={{fontSize: 30, color: '#444444'}}/>
                    </TouchableOpacity>
                </View>*/}
                {/*Logo*/}
                <View style={{flex:2}}>
                    <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                        <FadeInView>
                            <Image source={require('./images/50k-logo.png')} style={{width: 250, height: 250}} resizeMode="contain"/>
                        </FadeInView>
                    </View>
                </View>
                {/*Sign up form*/}
                <View style={{flex: 2, width: deviceWidth - 60, flexDirection: 'column', justifyContent: 'flex-end', marginBottom: 50}}>

                    <TextInput style={styles.textbox} placeholder="Email" keyboardType="email-address" underlineColorAndroid='transparent'  onChangeText={(text)=> this.setState({email: text})}/>
                    <TextInput style={styles.textbox} placeholder="Mobile" keyboardType="mobile" underlineColorAndroid='transparent' onChangeText={(text)=> this.setState({mobile: text})}/>


                    <Button  style={{backgroundColor: theme.themeColor, justifyContent: 'center', alignSelf: 'center', marginTop: 20, borderRadius: 5, width: deviceWidth - 60 }} full  onPress={this.verifyEmailAndMobileForSignIn}>

                        {this.state.isLoading ? <ActivityIndicator size={'small'} color={'white'}/> :<Text style={{color:'white'}}>Login</Text>}

                    </Button>
                    </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    textbox: {
        borderRadius: 5,
        height: 40,
        borderColor: '#dadada',
        borderWidth: 1,
        margin: 10,
        backgroundColor: '#f8f8f8',
        textAlign: 'center',
    }
});

